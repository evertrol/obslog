# ObsLog: An Observation Log application for Django

ObsLog provides a simple way to enter and store observation logs
through a database.

Currently, data stored are the (start of the) night, the observers,
general observer comments and the list of observations.

## Installation

ObsLog requires Python 3 (though it should be compatible with Python
2), Django, django-markdownx and django-simple-history.

ObsLog also relies on the Django default admin being enabled. There
is, however, no need to create a user for every observer, as the
observers can be defined independently inside the ObsLog entry form

You can install ObsLog with `setup.py` once downloaded, or directly
with `pip` from its repository:

    pip install git+https://github.com/GOTO/obslog


Once installed, you need to set up your project to include obslog and
its dependencies:

- add obslog, simple_history and markdownx to your `INSTALLED_APPS` in
  your project settings. *Make sure* that markdownx is added *after*
  obslog: this ensures the correct editing widget is picked up in the
  admin.

- add `'simple_history.middleware.HistoryRequestMiddleware'` to
  `MIDDLEWARE` in your project settings.

- add the markdown extra extensions setting (assuming you want to use
  those) to your project settings:

```python
MARKDOWNX_MARKDOWN_EXTENSIONS = [
    'markdown.extensions.extra'
]
```

- Add markdownx to your main urls, so that the admin and your app can
  make use of it:

```python
from markdownx import urls as markdownx

urlpatterns = [
    ...
    url(r'^markdownx/', include(markdownx)),
	...
]
```

- run `migrate` and `collectstatic` for your project, and you should
  be good to go.

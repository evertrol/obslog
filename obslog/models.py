from django.db import models
from django.utils.safestring import mark_safe
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
from simple_history.models import HistoricalRecords


__all__ = ['Observer', 'ObsLog']


class Observer(models.Model):
    name = models.CharField(max_length=160,
                            help_text=("Observer name. Use any name that uniquely "
                                       "identifies the observer"))
    def __str__(self):
        return self.name


class ObsLog(models.Model):
    date = models.DateField(help_text=mark_safe("<em>Start</em> of the night"))
    observers = models.ManyToManyField(Observer)
    comments = MarkdownxField(help_text="Observer log/comments. Use Markdown for layout.")
    observations = models.TextField(help_text=(
        "List of observations (copied from database). "
        "Use 'None' if no observations were taken"))
    history = HistoricalRecords()

    def __str__(self):
        return "Log for {}".format(self.date)

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('obslog:detail', args=[str(self.id)])

    def formatted_comments(self):
        return mark_safe(markdownify(self.comments))

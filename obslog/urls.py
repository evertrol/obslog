from django.conf.urls import include, url
from .views import *


urlpatterns = [
    url(r'^log/(?P<pk>\d+)/$',
        ObsLogView.as_view(),
        name='detail'),
    url(r'^night/(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/$',
        NightView.as_view(),
        name='night-day'),
    url(r'^night/(?P<year>\d{4})/(?P<month>\d{1,2})/$',
        NightView.as_view(),
        name='night-month'),
    url(r'^night/(?P<year>\d{4})/$',
        NightView.as_view(),
        name='night-year'),
    url(r'^night/$',
        NightView.as_view(),
        name='night'),
    url(r'^$',
        IndexView.as_view(),
        name='index'),
]

from django.contrib import admin
from django.db.models import TextField
from markdownx.widgets import AdminMarkdownxWidget
from simple_history.admin import SimpleHistoryAdmin
from .models import *


@admin.register(Observer)
class ObserverAdmin(admin.ModelAdmin):
    ordering = ('name',)



@admin.register(ObsLog)
class ObsLogAdmin(SimpleHistoryAdmin):
    ordering = ('date',)

from django.apps import AppConfig


class ObslogConfig(AppConfig):
    name = 'obslog'

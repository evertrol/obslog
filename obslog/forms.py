from django.forms import ModelForm
from .models import ObsLog


class ObsLogForm(ModelForm):
    class Meta:
        model = ObsLog
        exclude = []

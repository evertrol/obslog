from django.views.generic import TemplateView, DetailView, ListView
from django.views.generic.edit import FormView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import RedirectView
from django.urls import reverse_lazy
import datetime
import calendar
from collections import OrderedDict
from .models import *
from .forms import *


__all__ = ['IndexView', 'ObsLogCreate', 'ObsLogView', 'NightView']


class IndexView(LoginRequiredMixin, ListView):
    template_name = "obslog/index.html"
    model = ObsLog
    ordering = 'date'

class ObsLogCreate(LoginRequiredMixin, RedirectView):
    url=reverse_lazy('admin:obslog_obslog_add')


class ObsLogView(LoginRequiredMixin, DetailView):
    template_name = "obslog/log.html"
    model = ObsLog


class NightView(LoginRequiredMixin, ListView):
    template_name = "obslog/night.html"
    model = ObsLog
    ordering = 'date'

    def get_queryset(self):
        year = self.kwargs.get('year')
        if not year:
            return self.model.objects.all()
        month = self.kwargs.get('month')
        if not month:
            return self.model.objects.filter(date__year=year)
        day = self.kwargs.get('day')
        if not day:
            return self.model.objects.filter(
                date__year=year).filter(date__month=month)
        return self.model.objects.filter(
            date__year=year).filter(date__month=month).filter(date__day=day)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        month = self.kwargs.get('year')
        dates = self.model.objects.values_list(
            'date', flat=True).order_by('date')
        context['years'] = sorted(set(date.year for date in dates))
        year = self.kwargs.get('year')
        if not year:
            return context

        context['current_year'] = int(year)
        dates = self.model.objects.filter(date__year=year).values_list(
            'date', flat=True).order_by('date')
        months = sorted(set(date.month for date in dates))
        context['months'] = OrderedDict((month, calendar.month_name[month])
                                        for month in months)
        month = self.kwargs.get('month')
        if not month:
            return context

        context['current_month'] = calendar.month_name[int(month)]

        return context

#! /usr/bin/env python
"""Installation script"""

from setuptools import setup


setup(
    name='obslog',
    version='alpha',
    description='GOTO observation log web-app',
    license='FreeBSD',
    copyright='GOTO Observatory',
    author='Evert Rol',
    author_email='evert.rol@monash.nl',
    url='https://github.com/GOTO-OBS',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: FreeBSD License',
        'Programming Language :: Python',
    ],
    packages=['obslog', 'obslog.migrations'],
    package_dir={'obslog': 'obslog'},
    include_package_data=True,
    install_requires=['django', 'django-markdownx', 'django-simple-history']
)
